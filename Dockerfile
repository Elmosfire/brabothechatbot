FROM python:3.10.0-alpine

RUN apk add gcc g++ libc-dev

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt

COPY ./src /src
COPY config.yml config.yml

CMD python3 /src/main/bot.py
