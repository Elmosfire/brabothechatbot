# BraboTheChatBot

A rewrite of the famous bot Brabo in modern python

how to run: (assuming you're ssh'd into brabo box & brabo's repo is in `/home/brabo/brabothechatbot` & you need superuser privileges)
```
sudo su
cd /home/brabo/brabothechatbot
git pull
docker build -t brabo .
systemctl restart brabo
```
