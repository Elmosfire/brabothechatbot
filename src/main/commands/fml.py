import asyncio
from discord.ext import commands
import requests
from collections import deque
from bloom_filter2 import BloomFilter


class FmlCommands(commands.Cog):
    bloom = BloomFilter(
        max_elements=10 ** 5, error_rate=0.001, filename=("bloom.bin", -1)
    )

    def __init__(self, bot):
        self.bot = bot
        self.fmlcache = deque()

    def refresh_fmls(self):
        try:
            url = """http://www.fmylife.com/api/v2/article/list?page[number]=1&page[bypage]=20&status[]=4&orderby[RAND()]=ASC"""
            headers = {"X-VDM-Api-Key": "4dccf019bdfac"}
            reply = requests.get(url, headers=headers).json()
            self.fmlcache.extend(reply["data"])
        except Exception as e:
            print(str(e))
            return

    def generate_fmls(self):
        while True:
            self.fill_queue(5, 20)
            yield self.fmlcache.popleft()

    def fill_queue(self, min_, buffer):
        if len(self.fmlcache) < min_:
            while len(self.fmlcache) < min_ + buffer:
                self.refresh_fmls()

    def fml_valid(self, nextFml):
        if not nextFml.startswith("Today"):
            return False

        if nextFml in self.bloom:
            return False
        else:
            self.bloom.add(nextFml)
            return True

    @commands.command(no_pm=False)
    async def fml(self, ctx):
        """prints a "funny" FML."""

        if len(self.fmlcache) < 5:
            self.refresh_fmls()

        assert len(self.fmlcache), "Cannot print fml cause cache is empty"

        for nextFml in self.generate_fmls():
            if not self.fml_valid(nextFml["content_hidden"]):
                continue
            nextFml.update(nextFml.get("metrics", {}))
            if nextFml["votes_up"] + nextFml["votes_down"] < 40000:
                continue
            msg = """{content_hidden}\nsource: <{url}>\n||:anguished: {votes_up}, :rolling_eyes: {votes_down}||""".format(
                **nextFml
            )
            await ctx.send(msg)
            break

        self.fill_queue(20, 100)
