import asyncio
from discord.ext import commands
import praw
from random import randrange

from itertools import count

from cachetools import LRUCache

# this post is returned if a valid post cannot be found for any reason
errorPost = {"stickied": False}


class RedditCommands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.doneids = LRUCache(1000)  # should be plenty to avoid reposts for a day

    @commands.command(no_pm=False)
    async def reddit(self, ctx, subreddit: str = "random", *_):
        try:
            await ctx.send("\n".join(self._reddit(subreddit)))
        except Exception as e:
            await ctx.send(
                f"something went wrong getting a post from /r/{subreddit}\n{e}"
            )
            raise e

    def _reddit(self, subreddit):
        """
            Returns a post from a subreddit of choice.
        """
        post = self.get_toppost(subreddit, limit=20)  # first try
        tries = 1
        while post is None and tries < 5:
            post = self.get_toppost(subreddit, limit=None)  # subsequent tries
            tries += 1

        if post is None:
            yield (
                f"/r/{subreddit} did not give any usable posts after multiple tries, I just gave up, sorry..."
            )
            return

        print(f"post: {post}")
        self.doneids[post.id] = 1
        yield (f"/r/{post.subreddit}: **{post.title}**")

        if post.is_self:
            retval = post.selftext
        else:
            retval = post.url

        if post.over_18:
            retval = f"|| {retval} ||"

        yield (retval)

        yield (f"source: <{post.shortlink}>")

    def get_toppost(self, subredditname, limit=20):
        subreddit = self.reddit.subreddit(subredditname)
        for submission in subreddit.top("day", limit=limit):
            if not submission.stickied and submission.id not in self.doneids:
                return submission

    def login(self, username, password, client="", token=""):
        valid = client and token
        if not valid:
            print("invalid reddit client or token")

        self.reddit = praw.Reddit(
            client_id=client,
            client_secret=token,
            username=username,
            password=password,
            user_agent="python3:brabo:scraper",
        )
        print("Logged in as (reddit): ", self.reddit.user.me())
