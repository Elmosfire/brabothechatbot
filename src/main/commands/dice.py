import random
import discord
from discord.ext import commands
import re

from commands.modules.diceroller import dieInterpreterWrapper as dieInterpreterWrapper


class DierollCommands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.showrolls = {}

    @commands.command(description="For when you wanna settle the score some other way")
    async def choose(self, ctx, *choices: str):
        """Chooses between multiple choices."""
        await ctx.send(random.choice(choices))

    @commands.command(no_pm=True, aliases=["TSR"])
    async def toggleShowrolls(self, ctx):
        """toggles whether I'm showing you all the separate rolls, or just the total"""
        if ctx.message.channel.id not in self.showrolls:
            self.showrolls[ctx.message.channel.id] = False
        self.showrolls[ctx.message.channel.id] = not self.showrolls[
            ctx.message.channel.id
        ]
        if self.showrolls[ctx.message.channel.id]:
            await ctx.send("Individual rolls visible.")
        else:
            await ctx.send("Individual rolls invisible.")

    @commands.command(no_pm=True)
    async def roll(self, ctx, *, msg: str):
        """example: roll 3d6\nNdX+Y, rolls N dice of size X, adds Y to the result"""

        if ctx.message.channel.id not in self.showrolls:
            self.showrolls[ctx.message.channel.id] = False

        expr, total, totalrolls = dieInterpreterWrapper(msg)

        await ctx.send(str(total))
        if self.showrolls[ctx.message.channel.id]:
            await ctx.send(str(totalrolls))
        return

    @commands.command(no_pm=True)
    async def rollchar(self, ctx):
        """roll the base stats for a normal D&D 3.5 character"""

        if ctx.message.channel.id not in self.showrolls:
            self.showrolls[ctx.message.channel.id] = False

        results = [0, 0, 0, 0, 0, 0]
        for i in range(6):
            temproll = [0, 0, 0, 0]
            for j in range(4):
                temproll[j] = random.randint(1, 6)
            temproll.sort()
            for j in range(3):
                results[i] += temproll[j + 1]
        await ctx.send(str(results))
