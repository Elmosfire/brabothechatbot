import asyncio
import random
from discord.ext import commands


class GmoCommands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.fmlcache = []

    @commands.command(no_pm=False)
    async def gmoji(self, ctx):
        """Replies with 'Good morning + random emoji'"""
        await ctx.send("Good morning " + str(random.choice(list(self.bot.emojis))))

    @commands.command(no_pm=False)
    async def gmo(self, ctx):
        """Replies with 'Good morning :)'"""
        await ctx.send("Good morning :)")

    @commands.command(no_pm=False)
    async def respects(self, ctx):
        """Replies with F emoji"""
        await ctx.send(":regional_indicator_f:")
