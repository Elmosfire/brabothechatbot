import re
import operator
from random import randrange, random
from functools import reduce
from math import sqrt, cos, log, pi

def tokenize(expression):
    expression = re.sub(r"[dhl)(*/+-]", lambda x: f" {x.group(0)} ", expression)
    tokens = [t for t in re.split(r" ", expression) if not t.isspace() and t]
    nested = nest_tokens(tokens)
    return nested


def nest_tokens(l):
    r = []
    while l:
        nextchar = l.pop(0)
        if nextchar == "(":
            r.append(nest_tokens(l))
        elif nextchar == ")":
            return r
        else:
            r.append(nextchar)
    return r


class DieInterpreter:
    operation_order = (("d", "h", "l"), ("*", "/"), ("+", "-"))
    operators = {}

    default_left = {"+": 0, "-": 0, "h": 2, "l": 2}
    default_right = {"d": 20, "h": 20, "l": 20}

    def __init__(self, expr):
        self.history = []
        self.operators = {
            "+": operator.add,
            "-": operator.sub,
            "*": operator.mul,
            "/": self.spec_div,
            "d": self.dice,
            "h": self.highest,
            "l": self.lowest,
        }
        self.expr = expr

    @staticmethod
    def spec_div(a, b):
        return max(a // b, 1)

    def dice(self, a, b):
        if a > 10000:
            mean_ = a * (b + 1) / 2
            var_ = a * (b * (b + 1) * (2 * b + 1)) / 2 - mean_
            roll = cos(2 * pi * random()) * sqrt(-2 * log(random()))
            self.history.append(roll)
            return int(roll * sqrt(var_) + mean_)
        else:
            return sum(self.rolldie(b) for _ in range(a))

    def rolldie(self, r):
        s = randrange(r) + 1
        self.history.append(s)
        return s

    def highest(self, a, b):
        return max(self.rolldie(b) for _ in range(a))

    def lowest(self, a, b):
        return min(self.rolldie(b) for _ in range(a))

    def get_operator_order(self, m):
        return min(i for i, x in enumerate(self.operation_order) if m in x)

    def scan(self, l: list, soft=False):

        # recurse
        for i, _ in enumerate(l):
            if isinstance(l[i], list):
                l[i] = self.scan(l[i], soft)
        # remove invalid operators

        # get numbers
        for i, _ in enumerate(l):
            if isinstance(l[i], str) and l[i].isdigit():
                l[i] = int(l[i])
        l = list(filter(lambda x: (not isinstance(x, str)) or x in self.operators, l))

        # if first element is + or -, a number 0 is added
        if isinstance(l[0], str):
            l.insert(0, self.default_left.get(l[0], 1))
        # for double operators, the default of the right one is added

        i = 0
        while i < len(l) - 1:
            if isinstance(l[i], str) and isinstance(l[i + 1], str) or False:
                if l[i + 1] in "+-" and l[i] not in "dhl" and i + 2 < len(l):
                    s = l.pop(i + 1)
                    if s == "-":
                        l[i + 1] *= -1
                    break
                right = self.get_operator_order(l[i]) > self.get_operator_order(
                    l[i + 1]
                )
                loc = i + right
                default = self.default_left.get(l[loc], 1)
                if not right:
                    default = self.default_right.get(l[loc], default)
                print(l[loc], right, default, self.default_right.get(l[loc], ...))
                l.insert(i + 1, default)
            i += 1
        if isinstance(l[-1], str):
            l.append(self.default_right.get(l[-1], self.default_left.get(l[-1], 1)))

        if soft:
            return l
        for order in self.operation_order:
            while any(op in l for op in order):
                for i, _ in enumerate(l):
                    if l[i] in order:
                        op = l.pop(i)
                        loc = i

                        loc -= 1
                        a, b = l.pop(loc), l.pop(loc)
                        l.insert(loc, self.operators[op](a, b))
                        print(a, op, b)

                        break

        return reduce(operator.mul, l, 1)

    def solve(self):
        n = self.scan(tokenize(self.expr))
        # print("history",HISTORY)
        return n, self.history

    def show(self):
        return self.interpret(self.scan(tokenize(self.expr), soft=True))

    def interpret(self, tokens):
        return "".join(
            f"({self.interpret(t)})" if isinstance(t, list) else str(t) for t in tokens
        )


def dieInterpreterWrapper(expr):
    di = DieInterpreter(expr)
    return (di.show(), *di.solve())


if __name__ == "__main__":
    print("\n".join(map(str, dieInterpreterWrapper(input(">")))))
