import sqlite3
from collections.abc import MutableMapping

initsql = """
PRAGMA foreign_keys = ON;

CREATE TABLE IF NOT EXISTS hangmanScores(   
    username text CONSTRAINT hmscores_pk PRIMARY KEY, 
    score integer
);

CREATE TABLE IF NOT EXISTS hangmanGames(
    word text,
    used text,
    wrong integer,
    winner text,
    participants text,
    startdate text,
    enddate text
);

CREATE TABLE IF NOT EXISTS emojiBlacklist(
    character text PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS keyvals(
    key text PRIMARY KEY,
    val text
);
"""


class Database(MutableMapping):
    def __init__(self, test=False):
        self.test = test

    def __enter__(self):
        self.connect()
        self.c.executescript(initsql)
        self.commit = self.connection.commit
        self.commit()
        return self

    def __exit__(self):
        self.disconnect()

    def connect(self):
        # doesn't really do much but maybe useful
        if self.test:
            self.connection = sqlite3.connect("persist/brabo.db")
        else:
            self.connection = sqlite3.connect("/persist/brabo.db")
        self.c = self.connection.cursor()

    def disconnect(self):
        # doesn't do much but maybe usefull
        self.connection.close()
        self.c = None
        self.connection = None

    def insert_stats_line(self, message):
        return str(
            [
                message.author.name,
                message.channel.name,
                message.guild.name if message.guild else None,
            ]
        )

    def inserthm(self, hmgame):
        # debug things
        print(hmgame)

        # update score of the winner
        print("winner:")
        print(str(hmgame.winner))
        self.c.execute(
            """SELECT score FROM hangmanScores WHERE username = :user""",
            {"user": str(hmgame.winner.id)},
        )
        out = self.c.fetchone()
        print("out = " + str(out))
        if type(out) is type(None):
            score = None
        else:
            score = out[0]
        print("score:")
        print(score)
        if type(score) is int:
            score = score + 1
            self.c.execute(
                """UPDATE hangmanScores SET score = :score WHERE username = :user""",
                {"user": str(hmgame.winner.id), "score": score},
            )
        else:
            score = 1
            self.c.execute(
                """INSERT INTO hangmanScores VALUES (:user, :score)""",
                {"user": str(hmgame.winner.id), "score": score},
            )

        self.commit()

        # build players string
        playersStr = ""
        for user in hmgame.participants:
            playersStr = playersStr + ", " + str(user.id)

        # build "used" string
        used = ""
        for letter in hmgame.usedletters:
            used = used + ", " + letter

        # push game to database for storage
        self.c.execute(
            """INSERT INTO hangmanGames VALUES (:word, :used, :wrong, :winner, :participants, :startdate, :enddate)""",
            {
                "word": hmgame.word,
                "used": used,
                "wrong": hmgame.wrong,
                "winner": str(hmgame.winner.id),
                "participants": playersStr,
                "startdate": hmgame.startDate,
                "enddate": hmgame.endDate,
            },
        )
        self.commit()

    def select_score(self, username):
        self.c.execute(
            """SELECT score FROM hangmanScores WHERE username = :user""",
            {"user": username},
        )
        score = self.c.fetchone()[0]
        self.disconnect()
        if type(score) is int:
            return score
        else:
            return -1

    def select_top_scores(self):
        self.c.execute(
            """SELECT score, username FROM hangmanScores ORDER BY score DESC LIMIT 4"""
        )
        yield from self.c

    # checks whehthehr $char is in the blacklist for characters
    def is_blacklist_char(self, char):
        print("checking blacklisted chars for '" + char + "'")

        self.c.execute(
            """SELECT character FROM emojiBlacklist WHERE character = :char""",
            {"char": char},
        )
        if self.c.fetchone() == None:
            # if none are returned it's not in the blacklist
            return False
        else:
            return True

    # adds character $char to the blacklist
    def add_blacklist_char(self, char):
        print("adding to blacklist: '" + char + "'")
        self.c.execute("""INSERT INTO emojiBlacklist VALUES (:char)""", {"char": char})
        self.commit()
        return

    def __getitem__(self, key):
        self.c.execute("""SELECT val FROM keyvals WHERE key = :key""", {"key": key})
        for result in self.c:
            return result[0]
        return None

    def __setitem__(self, key, value):
        self.c.execute(
            """INSERT OR REPLACE INTO keyvals (key, val) VALUES (:key, :val)""",
            {"key": key, "val": value},
        )
        self.commit()

    def __delitem__(self, key):
        self.c.execute("""DELETE FROM keyvals WHERE key = :key""", {"key": key})
        self.commit()

    def __len__(self):
        self.c.execute("""SELECT COUNT(*) FROM keyvals""")
        return self.c.fetchone()

    def keys(self):
        self.c.execute("""SELECT key FROM keyvals""")
        return map(lambda x: x[0], self.c)

    def values(self):
        self.c.execute("""SELECT val FROM keyvals""")
        return map(lambda x: x[0], self.c)

    def items(self):
        self.c.execute("""SELECT key, val FROM keyvals""")
        yield from self.c

    def __contains__(self, key):
        self.c.execute("""SELECT key FROM keyvals where key = :key""", {"key"})
        out = self.c.fetchone()
        return out > 0

    def __iter__(self):
        self.c.execute("""SELECT val FROM keyvals""")
        yield from self.c
