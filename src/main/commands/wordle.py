import asyncio
from discord.ext import commands
import requests
import datetime
from random import random, shuffle


class WordleCommands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @staticmethod
    def generate_wordle_line(yellow_odds, green_odds, previous="⬛" * 5):
        # get all the non green squares
        free = [i for i, x in enumerate(previous) if x != "🟩"]
        free_mixed = free.copy()
        # if there is only one free square, this thing cannot be yellow
        if len(free) == 1:
            yield from "🟩🟩🟩🟩🟩"
            return

        # shuffle all the non green squares
        while any(a == b for a, b in zip(free, free_mixed)):
            shuffle(free_mixed)
        permutation = {unmixed: mixed for unmixed, mixed in zip(free, free_mixed)}
        # build the base again
        previous_mixed = "".join(previous[permutation.get(i, i)] for i in range(5))

        # upgrade fields if neccesairy.
        for x in previous_mixed:
            if x == "🟩":
                yield "🟩"
            elif x == "🟨":
                if random() < green_odds:
                    yield "🟩"
                else:
                    yield "🟨"
            else:
                if random() < yellow_odds:
                    yield "🟨"
                else:
                    yield "⬛"

    @staticmethod
    def generate_wordle_solution(yellow_odds, green_odds):
        current = "⬛" * 5
        history = []
        while True:
            if current == "🟩🟩🟩🟩🟩":
                break
            if len(history) == 6:
                break
            current = "".join(
                WordleCommands.generate_wordle_line(yellow_odds, green_odds, current)
            )
            if current.count("🟩") == 4 and current.count("🟨") == 1:
                current = "🟩🟩🟩🟩🟩"
            history.append(current)

        history_joined = "\n".join(history)
        return f"""{len(history)}/6\n\n{history_joined}"""

    @commands.command(no_pm=False)
    async def wordle(self, ctx):
        """Print's the bot's wordle results of today"""

        day = (datetime.datetime.utcnow() - datetime.datetime(2021, 6, 19)).days

        sol = self.generate_wordle_solution(0.3, 0.5)

        msg = f"""Wordle {day} {sol}"""

        await ctx.send(msg)
