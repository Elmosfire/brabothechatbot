import discord
from discord.ext import commands
import random
import string
import commands.database as database
from pprint import pprint


class VoteCommands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.group()
    async def Kappa(self, ctx):
        await ctx.send(":Kappa:")

    # @commands.group(invoke_without_command=True)
    @commands.group()
    async def testunicode(self, ctx):
        await ctx.send("adding 😁 to your message")
        await ctx.message.add_reaction("😁")
        await ctx.send("added 😁 to your message")

    @commands.group()
    async def testallemojis(self, ctx):
        await ctx.send("adding ten emojis to your message")
        for i in random.choices(list(self.bot.emojis), k=10):
            await ctx.message.add_reaction(i)
        await ctx.send("added ten emojis to your message")

    @commands.group()
    async def vote(self, ctx):
        db = self.bot.db
        pprint(ctx.message.content)
        emojis_to_add = []
        seen = set()
        for i, char in enumerate(ctx.message.content):
            if not db.is_blacklist_char(char) and char not in seen:
                emojis_to_add.append((i, char))
                seen |= {char}
        # iterate twice, because some emojis are unicode en get added above, and custom emojis get added now
        print("checked unicode, now custom")
        for emoji in self.bot.emojis:
            f = ctx.message.content.find(str(emoji))
            if f > 0:
                emojis_to_add.append((f, emoji))

        for _, emoji in sorted(emojis_to_add, key=lambda x: x[0]):
            try:
                print(f"trying to add {emoji}")
                await ctx.message.add_reaction(emoji)
            except Exception as e:
                print(type(e), e)
                print(f"'{emoji}'")
                print("why")
                db.add_blacklist_char(emoji)
