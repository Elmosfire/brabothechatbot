from unittest.mock import MagicMock
import asyncio
from functools import partial
from discord.ext.commands.bot import StringView, Context
import discord
from bot import bot


def create_message(content):
    message = MagicMock()
    message.content = content
    message.author = MagicMock()
    message.author.id = 0
    return message


class InterfaceBot:
    def __init__(self, bot):
        self.bot = bot

        self.user = MagicMock()
        self.user.id = -1
        self.user.mention = ""

        type(self.bot).user = self.user

        self.output = []
        self.error_flag = None

    async def send(self, message):
        # print(message)
        self.output.append(message)

    def raise_error(self, ctx, e):
        raise e

    async def mock_run(self, message):

        ctx = await self.bot.get_context(message)
        ctx.send = self.send
        if ctx.command is not None:
            ctx.command.dispatch_error = self.raise_error

        await self.bot.invoke(ctx)

    def run_command(self, message):
        self.output = []

        asyncio.run(self.mock_run(create_message(message)))

        return self.output


if __name__ == "__main__":
    b = InterfaceBot(bot)
    print(b.run_command("%gmo"))
