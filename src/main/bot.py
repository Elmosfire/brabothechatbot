#!/bin/python3
import discord

import asyncio

import commands.gmo as gmo
import commands.fml as fml
import commands.dice as dice
import commands.wordle as wordle
import commands.votes as votes
import commands.database as database
import commands.reddit as reddit

import sys

import yaml

from discord.ext import commands
from discord import Intents

guild_blacklist = ["71628177541177344"]
channel_whitelist = ["277834580503953409"]

def open_config():
    filename = "config.yml"
    if len(sys.argv) > 1:  # there's a command-line argument
        filename = sys.argv[1]
    
    with open(filename) as file:
        return yaml.safe_load(file)

if __name__ == "__main__":
    config = open_config()
    
    intents = Intents.default()
    intents.message_content = True

    bot = commands.Bot(
        command_prefix=commands.when_mentioned_or(config.get("trigger", "%")),
        description="Brabo the CHATBOT :D",
        intents=intents
    )
    
    @bot.event
    async def on_message(message):
        # white/blacklist here:
        if (
            message.guild is not None
            and message.guild.id in guild_blacklist
            and message.channel.id not in channel_whitelist
        ):
            return
        await bot.process_commands(message)

    asyncio.run(bot.add_cog(gmo.GmoCommands(bot)))
    asyncio.run(bot.add_cog(fml.FmlCommands(bot)))
    asyncio.run(bot.add_cog(dice.DierollCommands(bot)))
    asyncio.run(bot.add_cog(wordle.WordleCommands(bot)))
    asyncio.run(bot.add_cog(votes.VoteCommands(bot)))
    if config:
        if "reddit" in config:
            asyncio.run(bot.add_cog(reddit.RedditCommands(bot)))
            bot.reddit = reddit.RedditScraper(bot)
            rconfig = config["reddit"]
            bot.reddit.login(
                rconfig["username"],
                rconfig["password"],
                rconfig["client"],
                rconfig["token"],
            )
        if "token" in config:
            with database.Database() as db:
                bot.db = db
                bot.run(config["token"])

    else:
        raise AssertionError("Could not get a token from a config file.")
