import unittest
import sys, os
from pathlib import Path

# TODO no clue how to do this cleanly. Should be easy. Isn't
sys.path.append(str(Path(os.path.realpath(__file__)).parent.parent / "main"))

from magicmock_bot import InterfaceBot
from bot import bot

if __name__ == "__main__":
    b = InterfaceBot(bot)

    while True:
        for x in b.run_command(input(">> ")):
            print(x)
