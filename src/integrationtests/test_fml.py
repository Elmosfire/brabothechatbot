import unittest
import sys, os
from pathlib import Path
from itertools import count
from discord.ext.commands.errors import CommandInvokeError

# TODO no clue how to do this cleanly. Should be easy. Isn't
sys.path.append(str(Path(os.path.realpath(__file__)).parent.parent / "main"))

from magicmock_bot import InterfaceBot
from bot import bot

from commands.fml import FmlCommands, BloomFilter


# disconnect bloom filter from fmylife.com
def noop(*args):
    pass


class TestFml(unittest.TestCase):
    def test_cog_access(self):
        testbot = InterfaceBot(bot)
        self.assertIsInstance(testbot.bot.cogs["FmlCommands"], FmlCommands)

    def exhaust_fml(self, cache):
        testbot = InterfaceBot(bot)
        testbot.bot.cogs["FmlCommands"].bloom = BloomFilter(
            max_elements=10 ** 5, error_rate=0.001
        )
        testbot.bot.cogs["FmlCommands"].refresh_fmls = noop
        testbot.bot.cogs["FmlCommands"].fill_queue = noop
        testbot.bot.cogs["FmlCommands"].fmlcache.extend(
            [
                {"content_hidden": x, "url": "", "votes_up": 30000, "votes_down": 50000}
                for x in cache
            ]
        )
        all_messages = []
        while True:
            try:
                all_messages.append(testbot.run_command("%fml"))
            except CommandInvokeError as e:
                print(str(e))
                break
        return all_messages

    def verify_number(self, cache, expected):
        all_messages = self.exhaust_fml(cache)
        self.assertEqual(
            len(all_messages),
            expected,
            msg=f"""{all_messages} does not have {expected} elements""",
        )

    def test_direct_count(self):
        self.verify_number(
            [
                "Today, I had one fml, FML",
                "Today, I had another fml, FML",
                "Today, I didnt have an fml, FML",
            ],
            3,
        )

    def test_bloom_filter_count(self):
        self.verify_number(
            [
                "Today, I had one fml, FML",
                "Today, I had one fml, FML",
                "Today, I had another fml, FML",
            ],
            2,
        )

    def test_invalid_count(self):
        self.verify_number(
            [
                "Today, I had one fml, FML",
                "Today, I had another fml, FML",
                "Yesterday, I had one fml, FML",
            ],
            2,
        )


if __name__ == "__main__":
    unittest.main()
