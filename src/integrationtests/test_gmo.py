import unittest
import sys, os
from pathlib import Path

# TODO no clue how to do this cleanly. Should be easy. Isn't
sys.path.append(str(Path(os.path.realpath(__file__)).parent.parent / "main"))

from magicmock_bot import InterfaceBot
from bot import bot


class TestGmo(unittest.TestCase):
    def test_direct(self):
        testbot = InterfaceBot(bot)
        result = testbot.run_command("%gmo")

        self.assertEqual(len(result), 1, msg="Wrong number of  return messages")
        self.assertEqual(result[0], "Good morning :)")


if __name__ == "__main__":
    unittest.main()
